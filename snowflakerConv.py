from scipy.signal import convolve2d
from scipy.ndimage.interpolation import affine_transform
from PIL import Image
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import os
import sys
import shutil
import glob
import random
import math
import argparse
import configparser



'''
DEPENDENCIES:
pip3 install --upgrade pillow tqdm numpy scipy matplotlib
'''

# NOTE: Use of scipy and each convolutional call's respective kernel shape
# first implemented by Richard Cooper at https://github.com/richardcooper/snowflakes

class Snowflake:

    def __init__(self, name, rho, alpha, beta, theta, kappa, mu,
                 gamma, sigma, dimension, lifetime, snap_interval, color, animation_duration, dir_name):
        self.name = name
        self.rho = rho
        self.alpha = alpha
        self.beta = beta
        self.theta = theta
        self.kappa = kappa
        self.mu = mu
        self.gamma = gamma
        self.sigma = sigma
        self.dimension = dimension
        self.lifetime = lifetime
        self.snap_interval = snap_interval
        self.animation_duration = animation_duration
        self.color = color
        self.dir_name = dir_name

        # Initialize kernels, arrays, and starting state
        self.neighbor_kernel = np.array([[1, 1, 0], [1, 1, 1], [0, 1, 1]])
        self.boundary_kernel = np.array([[1, 1, 0], [1, -7, 1], [0, 1, 1]])
        self.d_mass_kernel = np.array([[1/7, 1/7, 0], [1/7, 1/7, 1/7], [0, 1/7, 1/7]])
        self.frozen_neighbors = None
        self.boundary = None

        self.attachment_flags = np.full(
            (dimension, dimension), False)
        self.boundary_mass = np.zeros(
            (dimension, dimension))
        self.crystal_mass = np.zeros(
            (dimension, dimension))
        self.diffusive_mass = np.full(
            (dimension, dimension), rho)

        self.attachment_flags[dimension // 2][dimension // 2] = True
        self.crystal_mass[dimension // 2][dimension // 2] = 1
        self.diffusive_mass[dimension // 2][dimension // 2] = 0

    def print_config(self):

        print("-" * (len(self.name) + 4))
        print(f"| {self.name.upper()} | ")
        print("-" * (len(self.name) + 4))
        print(f"Rho: {self.rho}")
        print(f"Beta: {self.beta}")
        print(f"Alpha: {self.alpha}")
        print(f"Theta: {self.theta}")
        print(f"Kappa: {self.kappa}")
        print(f"Mu: {self.mu}")
        print(f"Gamma: {self.gamma}")
        print(f"Sigma: {self.sigma}")
        print(f"Lattice (W, H): ({self.dimension}, {self.dimension})")
        print(f"Growth Lifetime: {self.lifetime}")
        print(f"Snapshot Interval: {self.snap_interval}")
        print(f"Animation Duration: {self.animation_duration}s")
        print(f"Color:", "Yes" if (self.color in ["y", "yes", "Yes", "YES"]) else "No")
        print()

    def grow(self):

        print("--------------------")
        print("GROWING SNOWFLAKE...")
        print("--------------------")

        for time_step in tqdm(range(self.lifetime), desc="Progress"):

            if time_step % self.snap_interval == 0 or time_step + 1 == self.lifetime:
                time_stamp = str(time_step).zfill(5)
                self.get_snapshot(time_stamp, self.dir_name)

            self.grow_step()

        self.animate_flake(self.dir_name)
        print()
        print("----- FINISHED! ------")
        print(f"Animation and snapshots are available in './figures/{self.dir_name}'")
        print('\a')

        self.show_snapshot(6)

    def grow_step(self):

        # Create proper boundary and frozen neighbor mappings
        self.update_localities()

        # STEP 1:
        self.update_diffusion()
        # STEP 2:
        self.update_freezing()
        # STEP 3:
        self.update_attachment()
        # STEP 4:
        self.update_melting()
        # STEP 5: (Optional)
        if self.sigma > 0:
            self.update_noise()

    def get_snapshot(self, time_stamp, dir_name):

        snowflake_colour_map = LinearSegmentedColormap(
            "",
            {'red': ((0.0, 0.80, 0.80),
                     (0.5, 1.00, 0.80),
                     (1.0, 0.05, 0.05)),

             'green': ((0.0, 0.80, 0.80),
                       (0.5, 1.00, 1.00),
                       (1.0, 0.15, 0.15)),

             'blue': ((0.0, 0.80, 0.80),
                      (0.5, 1.00, 1.00),
                      (1.0, 0.55, 0.55))
             }
        )

        shear_30_degrees = np.array([[1, 0, 0],
                                     [0.5, 1, 0],
                                     [0, 0, 1]])
        squash = np.array([[1, 0, 0],
                           [0, 1/math.sqrt(2), 0],
                           [0, 0, 1]])

        color_map = snowflake_colour_map if (self.color in ["y", "yes", "Yes", "YES"]) else "gray"
        translate_to_origin = np.array([[1, 0, self.dimension / 2], [0, 1, self.dimension / 2], [0, 0, 1]])
        translate_to_centre = np.array([[1, 0, -self.dimension / 2], [0, 1, -self.dimension / 2], [0, 0, 1]])

        background = np.invert(self.attachment_flags) * (self.diffusive_mass / np.max(self.diffusive_mass))
        flake = self.attachment_flags * (self.crystal_mass / np.max(self.crystal_mass))
        image = flake - background

        # Transform to hexagonal grid by sheering the vertical axis over 30 deg and then squashing vertically

        convolution = translate_to_origin @ shear_30_degrees @ translate_to_centre
        transformed_image = affine_transform(image, convolution, cval=-1)

        plt.imsave(f"./figures/{dir_name}/snapshots/t{time_stamp}.jpg", transformed_image, cmap=color_map)

    def show_snapshot(self, size):

        snowflake_colour_map = LinearSegmentedColormap(
            "",
            {'red': ((0.0, 0.80, 0.80),
                     (0.5, 1.00, 0.80),
                     (1.0, 0.05, 0.05)),

             'green': ((0.0, 0.80, 0.80),
                       (0.5, 1.00, 1.00),
                       (1.0, 0.15, 0.15)),

             'blue': ((0.0, 0.80, 0.80),
                      (0.5, 1.00, 1.00),
                      (1.0, 0.55, 0.55))
             }
        )

        shear_30_degrees = np.array([[1, 0, 0],
                                     [0.5, 1, 0],
                                     [0, 0, 1]])
        squash = np.array([[1, 0, 0],
                           [0, 1 / math.sqrt(3), 0],
                           [0, 0, 1]])


        color_map = snowflake_colour_map if (self.color in ["y", "yes", "Yes", "YES"]) else "gray"
        translate_to_origin = np.array([[1, 0, self.dimension / 2], [0, 1, self.dimension / 2], [0, 0, 1]])
        translate_to_centre = np.array([[1, 0, -self.dimension / 2], [0, 1, -self.dimension / 2], [0, 0, 1]])

        background = np.invert(self.attachment_flags) * (self.diffusive_mass / np.max(self.diffusive_mass))
        flake = self.attachment_flags * (self.crystal_mass / np.max(self.crystal_mass))
        image = flake - background

        # Transform to hexagonal grid by sheering the vertical axis over 30 deg and then squashing vertically

        convolution = translate_to_origin @ shear_30_degrees @ translate_to_centre
        transformed_image = affine_transform(image, convolution, cval=-1)

        fig, ax = plt.subplots(1, 1, figsize=(size, size))
        ax.imshow(transformed_image, cmap=color_map)
        ax.set_axis_off()
        plt.show()
    '''
    '''
    def update_diffusion(self):
        self.diffusive_mass = convolve2d(self.diffusive_mass, self.d_mass_kernel, boundary='symm', mode='same') \
                              * np.invert(self.attachment_flags) + (self.frozen_neighbors * self.diffusive_mass / 7)
    '''

    '''
    def update_freezing(self):

        self.boundary_mass += self.boundary * self.diffusive_mass * (1 - self.kappa)
        self.crystal_mass += self.boundary * self.diffusive_mass * self.kappa
        self.diffusive_mass -= self.boundary * self.diffusive_mass

    '''
    '''
    def update_attachment(self):
        self.attachment_flags |= ((self.frozen_neighbors == 1) | (self.frozen_neighbors == 2)) & (self.boundary_mass >= self.beta)

        # CONDITION A
        self.attachment_flags |= (self.frozen_neighbors == 3) & (self.boundary_mass >= 1)

        # CONDITION B
        nearby_diffusive_mass = convolve2d(self.diffusive_mass, self.neighbor_kernel, boundary="symm", mode="same")
        self.attachment_flags |= (self.frozen_neighbors == 3) & (self.boundary_mass >= self.alpha) & (nearby_diffusive_mass < self.theta)

        # CONDITION C
        self.attachment_flags |= (self.frozen_neighbors >= 4)

        # Once a site is attached, its boundary mass becomes crystal mass:
        self.crystal_mass += self.boundary * self.attachment_flags * self.boundary_mass
        self.boundary_mass -= self.boundary * self.attachment_flags * self.boundary_mass

        # Update self with new cell localities
        self.update_localities()

    '''
    '''
    def update_melting(self):
        self.diffusive_mass += self.boundary * (self.boundary_mass * self.mu + self.crystal_mass * self.gamma)
        self.boundary_mass -= self.boundary * self.boundary_mass * self.mu
        self.crystal_mass -= self.boundary * self.crystal_mass * self.gamma

    '''

    '''
    def update_noise(self):
        self.diffusive_mass *= 1 + (self.sigma if random.random() <= 0.5 else (self.sigma * -1))

    '''
    '''
    def update_localities(self):

        # Set updated neighbors
        self.frozen_neighbors = convolve2d(self.attachment_flags, self.boundary_kernel, boundary="symm", mode="same")
        np.clip(self.frozen_neighbors, 0, 7, self.frozen_neighbors)

        # Set updated boundary
        self.boundary = np.clip(self.frozen_neighbors, 0, 1)

    '''
    '''
    def animate_flake(self, snowflake_name):

        file_list = glob.glob(f"./figures/{snowflake_name}/snapshots/*.jpg")
        file_list.sort()
        images = [Image.open(file_name) for file_name in file_list]

        duration = int((self.animation_duration * 1000) / len(images))
        images[0].save(f"./figures/{snowflake_name}/animation_{snowflake_name}.gif",
                       save_all=True, append_images=images[1:], optimize=False, duration=duration, loop=0)


def get_config(args):
    config = configparser.ConfigParser()

    dir_name = args.name.replace(" ", "_").replace("'", "").lower()
    if os.path.exists(f"./figures/{dir_name}"):
        choice = input("Flake folder with given name already exists. Overwrite? (yes/no): ")

        if choice in ["no", "n", "NO", "No"]:
            print("----- Exited Simulator ------")
            sys.exit(0)

        shutil.rmtree(f"./figures/{dir_name}")

    #Two calls so that Windows doesn't complain
    os.makedirs(f"./figures/{dir_name}")
    os.makedirs(f"./figures/{dir_name}/snapshots")

    if args.load_config is not None:
        if not os.path.exists(args.load_config):
            print()
            print("There is no configuration file with that name at the given location")
            sys.exit(0)

        config.read(args.load_config)
        default = config["DEFAULT"]
        return Snowflake(args.name, float(default["rho"]), float(default["alpha"]),
                         float(default["beta"]), float(default["theta"]), float(default["kappa"]),
                         float(default["mu"]), float(default["gamma"]), float(default["sigma"]),
                         int(default["dimension"]), int(default["lifetime"]), int(default["snap_interval"]),
                         default["color"], int(default["animation_duration"]), dir_name)
    else:
        file_name = "config.ini"

        config["DEFAULT"] = {
            "rho": args.rho,
            "beta": args.beta,
            "alpha": args.alpha,
            "theta": args.theta,
            "kappa": args.kappa,
            "mu": args.mu,
            "gamma": args.gamma,
            "sigma": args.sigma,
            "dimension": args.dimension,
            "lifetime": args.lifetime,
            "snap_interval": args.snap_interval,
            "animation_duration": args.animation_duration,
            "color": args.color
        }

        file = open(f"./figures/{dir_name}/{file_name}", "w")
        config.write(file)
        file.close()

        return Snowflake(args.name, args.rho, args.alpha, args.beta,
                         args.theta, args.kappa, args.mu, args.gamma,
                         args.sigma, args.dimension, args.lifetime,
                         args.snap_interval, args.color, args.animation_duration, dir_name)


def get_parser():
    parser = argparse.ArgumentParser(description='Processes snowflake generator arguments')
    parser.add_argument("--name", type=str, default="Generated Snowflake",
                        help="The name of your snowflake")
    parser.add_argument("--rho", type=float, default=0.4,
                        help="Initial diffusive mass for non-attached sites at t=0")
    parser.add_argument("--alpha", type=float, default=0.8,
                        help="Min boundary mass for a site with boundary mass < 1 and 3 attached neighbors to attach")
    parser.add_argument("--beta", type=float, default=1.3,
                        help="Min boundary mass for a site with 1 or 2 attached neighbors to attach")
    parser.add_argument("--theta", type=float, default=0.025,
                        help="Max diffusive mass for a site with boundary mass < 1 and 3 attached neighbors to attach")
    parser.add_argument("--kappa", type=float, default=0.003,
                        help="Proportion [0, 1] of diffusive mass at each boundary site that crystallizes")
    parser.add_argument("--mu", type=float, default=0.07,
                        help="Proportion [0, 1] of boundary mass at each boundary site that becomes diffusive mass")
    parser.add_argument("--gamma", type=float, default=0.00005,
                        help="Proportion [0, 1] of crystal mass at each boundary site that becomes diffusive mass")
    parser.add_argument("--sigma", type=float, default=0.0,
                        help="Random perturbation [0, 1] applied to the diffusive mass at each site")
    parser.add_argument("--dimension", type=int, default=700,
                        help="Lateral dimensions of the square lattice on which the snowflake will grow")
    parser.add_argument("--lifetime", type=int, default=2000,
                        help="Number of time steps the snowflake will spend growing where [1000 , 10000] is common")
    parser.add_argument("--snap_interval", type=int, default=50,
                        help="Number of time steps between each growth snapshot")
    parser.add_argument("--animation_duration", type=int, default=6,
                        help="Duration of the final flake growth animation (in seconds)")
    parser.add_argument("--color", type=str, choices=["y", "yes", "Yes", "YES"], default="no",
                        help="Whether or not to give color to the snowflake growth snapshots")
    parser.add_argument("--load_config", type=str,
                        help="Optional path for a snowflake configuration file")

    return parser


if __name__ == "__main__":

    parser = get_parser()
    args = parser.parse_args()

    snowflake = get_config(args)
    os.system('cls' if os.name == 'nt' else 'clear')
    snowflake.print_config()
    snowflake.grow()
