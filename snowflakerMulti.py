import numpy as np
import os
import shutil
import glob
import random
import argparse
import multiprocessing
from PIL import Image
from tqdm import tqdm
import matplotlib.pyplot as plt


'''
DEPENDENCIES:
- pip3 install --upgrade pillow
- pip3 install --upgrade tqdm
- pip3 install --upgrade numpy
'''


class Snowflake:

    def __init__(self, name, rho, alpha, beta, theta, kappa, mu,
                 gamma, sigma, dimension, lifetime, workers, workload, columns, snap_interval):
        self.name = name
        self.rho = rho
        self.alpha = alpha
        self.beta = beta
        self.theta = theta
        self.kappa = kappa
        self.mu = mu
        self.gamma = gamma
        self.sigma = sigma
        self.dimension = dimension
        self.lifetime = lifetime
        self.max_workers = min(workers, multiprocessing.cpu_count())
        self.workload_percent = workload
        self.workload_columns = columns
        self.snap_interval = snap_interval

        self.attachment_flags = np.zeros(
            (dimension, dimension), np.float64)
        self.boundary_mass = np.zeros(
            (dimension, dimension), np.float64)
        self.crystal_mass = np.zeros(
            (dimension, dimension), np.float64)
        self.diffusive_mass = np.full(
            (dimension, dimension), rho, np.float64)

        self.origin_x = dimension // 2
        self.origin_y = dimension // 2

        self.attachment_flags[dimension // 2][dimension // 2] = 1
        self.crystal_mass[dimension // 2][dimension // 2] = 1
        self.diffusive_mass[dimension // 2][dimension // 2] = 0

        self.next_attachment_flags = np.zeros(
            (dimension, dimension), np.float64)
        self.next_boundary_mass = np.zeros(
            (dimension, dimension), np.float64)
        self.next_crystal_mass = np.zeros(
            (dimension, dimension), np.float64)
        self.next_diffusive_mass = np.full(
            (dimension, dimension), rho, np.float64)

        self.next_attachment_flags[dimension // 2][dimension // 2] = 1
        self.next_crystal_mass[dimension // 2][dimension // 2] = 1
        self.next_diffusive_mass[dimension // 2][dimension // 2] = 0

    def print_config(self):

        print("-" * (len(self.name) + 4))
        print(f"| {self.name.upper()} | ")
        print("-" * (len(self.name) + 4))
        print(f"Rho: {self.rho}")
        print(f"Beta: {self.beta}")
        print(f"Alpha: {self.alpha}")
        print(f"Theta: {self.theta}")
        print(f"Kappa: {self.kappa}")
        print(f"Mu: {self.mu}")
        print(f"Gamma: {self.gamma}")
        print(f"Sigma: {self.sigma}")
        print(f"Lattice (W, H): ({self.dimension}, {self.dimension})")
        print(f"Growth Lifetime: {self.lifetime}")
        print(f"Available CPUs: {multiprocessing.cpu_count()} ({self.max_workers} in use)")
        print(f"Workload: {self.workload_percent}% per process ({self.workload_columns * self.dimension} cells)")
        print(f"Columns Per Process: {self.workload_columns}")
        print(f"Snapshot Interval: {self.snap_interval}")
        print()

    def grow(self):

        dir_name = self.name.replace(" ", "_").replace("'", "").lower()

        if os.path.exists(f"./figures/{dir_name}"):
            shutil.rmtree(f"./figures/{dir_name}")

        os.makedirs(f"./figures/{dir_name}/snapshots")

        print("--------------------")
        print("GROWING SNOWFLAKE...")
        print("--------------------")

        with multiprocessing.Pool(processes=self.max_workers) as pool:

            for time_step in tqdm(range(self.lifetime), desc="Progress"):

                if time_step % self.snap_interval == 0 or time_step + 1 == self.lifetime:
                    time_stamp = str(time_step).zfill(5)
                    plt.imsave(
                        f"./figures/{dir_name}/snapshots/t{time_stamp}.jpg", self.crystal_mass, cmap="gray")

                self.grow_step(pool)


            pool.close()

        self.animate_flake(dir_name)
        print()
        print("----- FINISHED! ------")
        print(f"Animation and snapshots are available in './figures/{dir_name}'")
        print()
        plt.imshow(self.crystal_mass, cmap="gray")
        plt.title(f"Crystal Mass at t={time_step}")
        plt.show()

    def grow_step(self, pool):

        # Make copy of arrays. Each cell needs access to state at t-1
        max_w = self.dimension - 1
        # attachment = np.copy(self.next_attachment_flags)
        # boundary = np.copy(self.next_boundary_mass)
        # crystal = np.copy(self.next_crystal_mass)
        # diffusive= np.copy(self.next_diffusive_mass)

        params = [x for x in range(1, max_w, self.workload_columns)]
        pool.map(self.grow_process, params)

        self.attachment_flags = None
        self.boundary_mass = None
        self.crystal_mass = None
        self.diffusive_mass = None

        self.attachment_flags = np.copy(self.next_attachment_flags)

        self.boundary_mass = np.copy(self.next_boundary_mass)
        self.crystal_mass = np.copy(self.next_crystal_mass)
        self.diffusive_mass = np.copy(self.next_diffusive_mass)

    '''
    '''
    def grow_process(self, x_start):
        max_h = self.dimension - 1
        max_w = self.dimension - 1

        x_span = min(self.workload_columns, max_w - x_start)
        for x in range(x_start, x_start + x_span):
            for y in range(1, max_h):
                attachment = self.attachment_flags[x][y]
                boundary = self.boundary_mass[x][y]
                crystal = self.crystal_mass[x][y]
                diffusive = self.diffusive_mass[x][y]

                # STEP 1:
                diffusive = self.update_diffusion(
                    x, y, attachment, diffusive)
                # STEP 2:
                boundary, crystal, diffusive = self.update_freezing(
                    x, y, boundary, crystal, diffusive)
                # STEP 3:
                attachment, boundary, crystal = self.update_attachment(
                    x, y, attachment, boundary, crystal)
                # STEP 4:
                boundary, crystal, diffusive = self.update_melting(
                    x, y, boundary, crystal, diffusive)
                # STEP 5: (Optional)
                diffusive = self.update_noise(diffusive)

                self.next_attachment_flags[x][y] = attachment
                self.next_boundary_mass[x][y] = boundary
                self.next_crystal_mass[x][y] = crystal
                self.next_diffusive_mass[x][y] = diffusive


    '''

    '''
    def update_diffusion(self, x, y, attachment, diffusive):
        if attachment == 1:  # diffusive mass only evolves on sites not yet frozen
            return diffusive

        neighbors_x = np.array([x, x + 1, x + 1, x, x - 1, x - 1])
        neighbors_y = np.array([y + 1, y, y - 1, y - 1, y, y + 1])

        next_diffusive = 0.0

        # TODO: Add x as well? x is in the set {x}U{neighbors of x}
        for x_pos, y_pos in zip(neighbors_x, neighbors_y):
            # some neighbor is frozen so (x,y) is boundary site
            if self.attachment_flags[x_pos][y_pos] == 1:
                next_diffusive += diffusive  # for frozen neighbors add center term
            else:
                # keep summing normal neighbor terms while no frozen neighbor found
                next_diffusive += self.diffusive_mass[x_pos][y_pos]
        next_diffusive = (1 / 7) * (next_diffusive + diffusive)  # + diffusive?

        return next_diffusive

    '''

    '''
    def update_freezing(self, x, y, boundary, crystal, diffusive):
        if not self.is_boundary(x, y):  # Freezing only occurs at boundary sites
            return boundary, crystal, diffusive

        next_boundary = boundary + ((1 - self.kappa) * diffusive)
        next_crystal = crystal + (self.kappa * diffusive)
        next_diffusive = 0

        return next_boundary, next_crystal, next_diffusive

    '''

    '''
    def update_attachment(self, x, y, attachment, boundary, crystal):
        if not self.is_boundary(x, y):  # Attachment only occurs at boundary sites
            return attachment, boundary, crystal

        neighbors_x = np.array([x, x + 1, x + 1, x, x - 1, x - 1])
        neighbors_y = np.array([y + 1, y, y - 1, y - 1, y, y + 1])

        attached_neighbors = self.n_attached_neighbors(x, y)
        neighbors_diffusive = 0.0

        for x_pos, y_pos in zip(neighbors_x, neighbors_y):
            neighbors_diffusive += self.diffusive_mass[x_pos][y_pos]

        # 1 or 2 attached neighbors
        condition_a = ((attached_neighbors == 1 or attached_neighbors == 2)
                       and boundary >= self.beta)

        # 3 attached neighbors
        condition_b = (attached_neighbors == 3 and
                       (boundary >= 1 or
                        (neighbors_diffusive < self.theta and boundary >= self.alpha)))
        # 4 or more attached neighbors
        condition_c = attached_neighbors >= 4

        # CASE 1: 1 or 2 neighbors
        if condition_a or condition_b or condition_c:
            print("CELL ATTACHED")
            next_attachment = 1
            next_crystal = crystal + boundary
            next_boundary = 0
            return next_attachment, next_boundary, next_crystal

        return attachment, boundary, crystal

    '''

    '''
    def update_melting(self, x, y, boundary, crystal, diffusive):

        if not self.is_boundary(x, y):  # Melting only occurs at boundary sites
            return boundary, crystal, diffusive

        next_boundary = (1 - self.mu) * boundary

        next_crystal = (1 - self.gamma) * crystal

        next_diffusive = diffusive + (self.mu * boundary) + (self.gamma * crystal)

        return next_boundary, next_crystal, next_diffusive

    '''

    '''
    def update_noise(self, diffusive):

        if self.sigma == 0:  # 0 sigma indicates deterministic flake
            return diffusive

        noise = random.random() <= 0.5 if self.sigma else (self.sigma * -1)

        next_diffusive = (1 + noise) * diffusive

        return next_diffusive

    '''
    
    '''
    def is_boundary(self, x, y):

        if self.attachment_flags[x][y] == 1:
            return False

        neighbors_x = np.array([x, x + 1, x + 1, x, x - 1, x - 1])
        neighbors_y = np.array([y + 1, y, y - 1, y - 1, y, y + 1])

        for x_pos, y_pos in zip(neighbors_x, neighbors_y):
            if self.attachment_flags[x_pos][y_pos] == 1:
                return True

        return False

    '''
    '''
    def n_attached_neighbors(self, x, y):
        neighbors_x = np.array([x, x + 1, x + 1, x, x - 1, x - 1])
        neighbors_y = np.array([y + 1, y, y - 1, y - 1, y, y + 1])

        n_attached = 0
        for x_pos, y_pos in zip(neighbors_x, neighbors_y):
            if self.attachment_flags[x_pos][y_pos] == 1:
                n_attached += 1

        return n_attached

    '''
    '''
    def animate_flake(self, snowflake_name):
        file_list = glob.glob(f"./figures/{snowflake_name}/snapshots/*.jpg")
        file_list.sort()

        images = [Image.open(file_name) for file_name in file_list]
        images[0].save(f"./figures/{snowflake_name}/animation_{snowflake_name}.gif",
                       save_all=True, append_images=images[1:], optimize=False, duration=200, loop=0)


'''
'''
def calc_workload(args_percentage, args_columns, dimension, workers):
    workload = args_columns
    columns = args_columns

    if args_columns is not None:  # Calculate optimal work distribution
        columns = args_columns
        workload = round((columns / dimension) * 100, 1)
    elif args_percentage is not None:
        workload = args_percentage
        columns = int(round(dimension / (100 / workload), 0))
    else:
        min_workload_cols = 20
        max_workload_cols = 55
        columns = max((dimension // workers), min_workload_cols)
        columns = min(columns, max_workload_cols)

        workload = (columns / dimension) * 100

    return round(workload, 1), columns


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("--name", type=str, default="Generated Snowflake",
                        help="The name of your snowflake")
    parser.add_argument("--rho", type=float, default=0.4,
                        help="Initial diffusive mass for non-attached sites at t=0")
    parser.add_argument("--alpha", type=float, default=0.8,
                        help="Min boundary mass for a site with boundary mass < 1 and 3 attached neighbors to attach")
    parser.add_argument("--beta", type=float, default=1.3,
                        help="Min boundary mass for a site with 1 or 2 attached neighbors to attach")
    parser.add_argument("--theta", type=float, default=0.025,
                        help="Max diffusive mass for a site with boundary mass < 1 and 3 attached neighbors to attach")
    parser.add_argument("--kappa", type=float, default=0.003,
                        help="Proportion [0, 1] of diffusive mass at each boundary site that crystallizes")
    parser.add_argument("--mu", type=float, default=0.07,
                        help="Proportion [0, 1] of boundary mass at each boundary site that becomes diffusive mass")
    parser.add_argument("--gamma", type=float, default=0.00005,
                        help="Proportion [0, 1] of crystal mass at each boundary site that becomes diffusive mass")
    parser.add_argument("--sigma", type=float, default=0.0,
                        help="Random perturbation [0, 1] applied to the diffusive mass at each site")
    parser.add_argument("--dimension", type=int, default=700,
                        help="Lateral dimensions of the square lattice on which the snowflake will grow")
    parser.add_argument("--lifetime", type=int, default=100,
                        help="Number of time steps the snowflake will spend growing where [1000 , 10000] is common")
    parser.add_argument("--workers", type=int, default=multiprocessing.cpu_count(),
                        help="Number of processes assigned to assist in snowflake growth computations")
    parser.add_argument("--workload_percent", type=int,
                        help="Percent of total computation that each process should undertake")
    parser.add_argument("--workload_columns", type=int,
                        help="Number of columns of the growth lattice that each process should work on")
    parser.add_argument("--snap_interval", type=int, default=50,
                        help="Number of time steps between each growth snapshot")

    args = parser.parse_args()

    '''
    Calculate the default workload percentage 
    '''
    workload, cols = calc_workload(args.workload_percent, args.workload_columns, args.dimension, args.workers)

    snowflake = Snowflake(args.name, args.rho, args.alpha, args.beta,
                          args.theta, args.kappa, args.mu, args.gamma,
                          args.sigma, args.dimension, args.lifetime, args.workers, workload, cols, args.snap_interval)
    os.system('cls' if os.name == 'nt' else 'clear')
    snowflake.print_config()
    snowflake.grow()